/*
Navicat MySQL Data Transfer

Source Server         : 连接1
Source Server Version : 80024
Source Host           : localhost:3306
Source Database       : django

Target Server Type    : MYSQL
Target Server Version : 80024
File Encoding         : 65001

Date: 2021-12-01 14:44:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_group_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_permission`
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_croatian_ci NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can view log entry', '1', 'view_logentry');
INSERT INTO `auth_permission` VALUES ('5', 'Can add permission', '2', 'add_permission');
INSERT INTO `auth_permission` VALUES ('6', 'Can change permission', '2', 'change_permission');
INSERT INTO `auth_permission` VALUES ('7', 'Can delete permission', '2', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('8', 'Can view permission', '2', 'view_permission');
INSERT INTO `auth_permission` VALUES ('9', 'Can add group', '3', 'add_group');
INSERT INTO `auth_permission` VALUES ('10', 'Can change group', '3', 'change_group');
INSERT INTO `auth_permission` VALUES ('11', 'Can delete group', '3', 'delete_group');
INSERT INTO `auth_permission` VALUES ('12', 'Can view group', '3', 'view_group');
INSERT INTO `auth_permission` VALUES ('13', 'Can add user', '4', 'add_user');
INSERT INTO `auth_permission` VALUES ('14', 'Can change user', '4', 'change_user');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete user', '4', 'delete_user');
INSERT INTO `auth_permission` VALUES ('16', 'Can view user', '4', 'view_user');
INSERT INTO `auth_permission` VALUES ('17', 'Can add content type', '5', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('18', 'Can change content type', '5', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('19', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('20', 'Can view content type', '5', 'view_contenttype');
INSERT INTO `auth_permission` VALUES ('21', 'Can add session', '6', 'add_session');
INSERT INTO `auth_permission` VALUES ('22', 'Can change session', '6', 'change_session');
INSERT INTO `auth_permission` VALUES ('23', 'Can delete session', '6', 'delete_session');
INSERT INTO `auth_permission` VALUES ('24', 'Can view session', '6', 'view_session');
INSERT INTO `auth_permission` VALUES ('25', 'Can add test', '7', 'add_test');
INSERT INTO `auth_permission` VALUES ('26', 'Can change test', '7', 'change_test');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete test', '7', 'delete_test');
INSERT INTO `auth_permission` VALUES ('28', 'Can view test', '7', 'view_test');
INSERT INTO `auth_permission` VALUES ('29', 'Can add 用户', '8', 'add_user');
INSERT INTO `auth_permission` VALUES ('30', 'Can change 用户', '8', 'change_user');
INSERT INTO `auth_permission` VALUES ('31', 'Can delete 用户', '8', 'delete_user');
INSERT INTO `auth_permission` VALUES ('32', 'Can view 用户', '8', 'view_user');
INSERT INTO `auth_permission` VALUES ('33', 'Can add overtime', '9', 'add_overtime');
INSERT INTO `auth_permission` VALUES ('34', 'Can change overtime', '9', 'change_overtime');
INSERT INTO `auth_permission` VALUES ('35', 'Can delete overtime', '9', 'delete_overtime');
INSERT INTO `auth_permission` VALUES ('36', 'Can view overtime', '9', 'view_overtime');
INSERT INTO `auth_permission` VALUES ('37', 'Can add countdown', '10', 'add_countdown');
INSERT INTO `auth_permission` VALUES ('38', 'Can change countdown', '10', 'change_countdown');
INSERT INTO `auth_permission` VALUES ('39', 'Can delete countdown', '10', 'delete_countdown');
INSERT INTO `auth_permission` VALUES ('40', 'Can view countdown', '10', 'view_countdown');

-- ----------------------------
-- Table structure for `auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8_croatian_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) COLLATE utf8_croatian_ci NOT NULL,
  `first_name` varchar(150) COLLATE utf8_croatian_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_croatian_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_croatian_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_user_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `coh_countdown`
-- ----------------------------
DROP TABLE IF EXISTS `coh_countdown`;
CREATE TABLE `coh_countdown` (
  `id` int NOT NULL AUTO_INCREMENT,
  `downtxt` varchar(256) COLLATE utf8_croatian_ci NOT NULL,
  `downdate` varchar(128) COLLATE utf8_croatian_ci NOT NULL,
  `pid` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `color_downdate` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `color_downtxt` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `color_theme` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of coh_countdown
-- ----------------------------
INSERT INTO `coh_countdown` VALUES ('1', '距离傻妞诞生还有', '2060-01-01 00:00:00', '2', '#1E9FFF', '#FF5722', '#393D49');

-- ----------------------------
-- Table structure for `coh_overtime`
-- ----------------------------
DROP TABLE IF EXISTS `coh_overtime`;
CREATE TABLE `coh_overtime` (
  `id` int NOT NULL AUTO_INCREMENT,
  `day` varchar(128) COLLATE utf8_croatian_ci NOT NULL,
  `begintime` varchar(256) COLLATE utf8_croatian_ci NOT NULL,
  `endtime` varchar(256) COLLATE utf8_croatian_ci NOT NULL,
  `hours` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `pid` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `week` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `month` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `year` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of coh_overtime
-- ----------------------------
INSERT INTO `coh_overtime` VALUES ('3', '周一', '2021-11-22 19:00:00', '2021-11-22 19:00:00', '0.00', '2', '47', '11', '2021');
INSERT INTO `coh_overtime` VALUES ('4', '周二', '2021-11-23 19:06:53', '2021-11-23 21:06:56', '2.00', '2', '47', '11', '2021');

-- ----------------------------
-- Table structure for `coh_test`
-- ----------------------------
DROP TABLE IF EXISTS `coh_test`;
CREATE TABLE `coh_test` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of coh_test
-- ----------------------------

-- ----------------------------
-- Table structure for `coh_user`
-- ----------------------------
DROP TABLE IF EXISTS `coh_user`;
CREATE TABLE `coh_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_croatian_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  `sex` varchar(32) COLLATE utf8_croatian_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_croatian_ci NOT NULL,
  `photo_file` varchar(100) COLLATE utf8_croatian_ci NOT NULL,
  `photo_name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of coh_user
-- ----------------------------
INSERT INTO `coh_user` VALUES ('1', '测', '123456', '男', 'deer_cui@163.com', 'photos/202111231854246566071.png', '202111231854246566071.png');
INSERT INTO `coh_user` VALUES ('2', '崔术森', '123456', '男', 'deer_cui@163.com', 'photos/default.jpg', 'default.jpg');
INSERT INTO `coh_user` VALUES ('3', '1', '1', '女', 'deer_cui@163.com', 'photos/default.jpg', 'default.jpg');

-- ----------------------------
-- Table structure for `django_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_croatian_ci,
  `object_repr` varchar(200) COLLATE utf8_croatian_ci NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext COLLATE utf8_croatian_ci NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for `django_content_type`
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) COLLATE utf8_croatian_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('4', 'auth', 'user');
INSERT INTO `django_content_type` VALUES ('10', 'COH', 'countdown');
INSERT INTO `django_content_type` VALUES ('9', 'COH', 'overtime');
INSERT INTO `django_content_type` VALUES ('7', 'COH', 'test');
INSERT INTO `django_content_type` VALUES ('8', 'COH', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('6', 'sessions', 'session');

-- ----------------------------
-- Table structure for `django_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) COLLATE utf8_croatian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_croatian_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'COH', '0001_initial', '2021-11-23 10:48:44.647882');
INSERT INTO `django_migrations` VALUES ('2', 'COH', '0002_auto_20201123_1153', '2021-11-23 10:48:44.904189');
INSERT INTO `django_migrations` VALUES ('3', 'COH', '0003_overtime', '2021-11-23 10:48:45.690088');
INSERT INTO `django_migrations` VALUES ('4', 'COH', '0004_auto_20201123_1442', '2021-11-23 10:48:47.366607');
INSERT INTO `django_migrations` VALUES ('5', 'COH', '0005_auto_20201123_1720', '2021-11-23 10:48:50.478303');
INSERT INTO `django_migrations` VALUES ('6', 'COH', '0006_overtime_pid', '2021-11-23 10:48:51.349960');
INSERT INTO `django_migrations` VALUES ('7', 'COH', '0007_overtime_week', '2021-11-23 10:48:52.352281');
INSERT INTO `django_migrations` VALUES ('8', 'COH', '0008_auto_20201124_1349', '2021-11-23 10:48:56.457310');
INSERT INTO `django_migrations` VALUES ('9', 'COH', '0009_countdown', '2021-11-23 10:48:57.940373');
INSERT INTO `django_migrations` VALUES ('10', 'COH', '0010_countdown_color', '2021-11-23 10:48:58.542735');
INSERT INTO `django_migrations` VALUES ('11', 'COH', '0011_auto_20201127_1129', '2021-11-23 10:48:58.634514');
INSERT INTO `django_migrations` VALUES ('12', 'COH', '0012_remove_countdown_color', '2021-11-23 10:49:00.921377');
INSERT INTO `django_migrations` VALUES ('13', 'COH', '0013_countdown_color', '2021-11-23 10:49:02.140119');
INSERT INTO `django_migrations` VALUES ('14', 'COH', '0014_remove_countdown_color', '2021-11-23 10:49:03.286057');
INSERT INTO `django_migrations` VALUES ('15', 'COH', '0015_countdown_color', '2021-11-23 10:49:03.685988');
INSERT INTO `django_migrations` VALUES ('16', 'COH', '0016_auto_20201127_1133', '2021-11-23 10:49:06.938294');
INSERT INTO `django_migrations` VALUES ('17', 'COH', '0017_countdown_color_theme', '2021-11-23 10:49:07.889752');
INSERT INTO `django_migrations` VALUES ('18', 'COH', '0018_auto_20201204_1513', '2021-11-23 10:49:10.364139');
INSERT INTO `django_migrations` VALUES ('19', 'COH', '0019_auto_20201204_1514', '2021-11-23 10:49:10.486811');
INSERT INTO `django_migrations` VALUES ('20', 'COH', '0020_auto_20201204_1516', '2021-11-23 10:49:16.597477');
INSERT INTO `django_migrations` VALUES ('21', 'COH', '0021_auto_20201204_1517', '2021-11-23 10:49:18.013692');
INSERT INTO `django_migrations` VALUES ('22', 'COH', '0022_auto_20201204_1517', '2021-11-23 10:49:20.877038');
INSERT INTO `django_migrations` VALUES ('23', 'COH', '0023_auto_20201204_1518', '2021-11-23 10:49:22.003029');
INSERT INTO `django_migrations` VALUES ('24', 'COH', '0024_auto_20201204_1519', '2021-11-23 10:49:25.470761');
INSERT INTO `django_migrations` VALUES ('25', 'COH', '0025_auto_20201204_1519', '2021-11-23 10:49:29.756512');
INSERT INTO `django_migrations` VALUES ('26', 'COH', '0026_overtime_month', '2021-11-23 10:49:30.229248');
INSERT INTO `django_migrations` VALUES ('27', 'COH', '0027_overtime_year', '2021-11-23 10:49:31.330277');
INSERT INTO `django_migrations` VALUES ('28', 'contenttypes', '0001_initial', '2021-11-23 10:49:33.004800');
INSERT INTO `django_migrations` VALUES ('29', 'auth', '0001_initial', '2021-11-23 10:49:56.621678');
INSERT INTO `django_migrations` VALUES ('30', 'admin', '0001_initial', '2021-11-23 10:50:01.165531');
INSERT INTO `django_migrations` VALUES ('31', 'admin', '0002_logentry_remove_auto_add', '2021-11-23 10:50:01.290228');
INSERT INTO `django_migrations` VALUES ('32', 'admin', '0003_logentry_add_action_flag_choices', '2021-11-23 10:50:01.655221');
INSERT INTO `django_migrations` VALUES ('33', 'contenttypes', '0002_remove_content_type_name', '2021-11-23 10:50:08.153942');
INSERT INTO `django_migrations` VALUES ('34', 'auth', '0002_alter_permission_name_max_length', '2021-11-23 10:50:10.469690');
INSERT INTO `django_migrations` VALUES ('35', 'auth', '0003_alter_user_email_max_length', '2021-11-23 10:50:12.854288');
INSERT INTO `django_migrations` VALUES ('36', 'auth', '0004_alter_user_username_opts', '2021-11-23 10:50:12.950061');
INSERT INTO `django_migrations` VALUES ('37', 'auth', '0005_alter_user_last_login_null', '2021-11-23 10:50:14.820047');
INSERT INTO `django_migrations` VALUES ('38', 'auth', '0006_require_contenttypes_0002', '2021-11-23 10:50:14.895831');
INSERT INTO `django_migrations` VALUES ('39', 'auth', '0007_alter_validators_add_error_messages', '2021-11-23 10:50:14.942738');
INSERT INTO `django_migrations` VALUES ('40', 'auth', '0008_alter_user_username_max_length', '2021-11-23 10:50:16.881524');
INSERT INTO `django_migrations` VALUES ('41', 'auth', '0009_alter_user_last_name_max_length', '2021-11-23 10:50:18.915116');
INSERT INTO `django_migrations` VALUES ('42', 'auth', '0010_alter_group_name_max_length', '2021-11-23 10:50:22.227237');
INSERT INTO `django_migrations` VALUES ('43', 'auth', '0011_update_proxy_permissions', '2021-11-23 10:50:22.404763');
INSERT INTO `django_migrations` VALUES ('44', 'auth', '0012_alter_user_first_name_max_length', '2021-11-23 10:50:25.234199');
INSERT INTO `django_migrations` VALUES ('45', 'sessions', '0001_initial', '2021-11-23 10:50:26.274419');

-- ----------------------------
-- Table structure for `django_session`
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_croatian_ci NOT NULL,
  `session_data` longtext COLLATE utf8_croatian_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('1j3g0tz0qrdnvhy2g3a8dvnscjst0clg', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MiwidXNlcm5hbWUiOiJcdTVkMTRcdTY3MmZcdTY4ZWUifQ:1mpTcA:WxH5l8-Pq8tYoe264UY1wgtl8_XvNYBV8qZEtcpE41s', '2021-12-07 11:05:30.650579');
INSERT INTO `django_session` VALUES ('e39he7srtoiklf8zd42tt68lpd6iqs6g', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MywidXNlcm5hbWUiOiIxIn0:1mpTbr:QWNX-2xIUz2yayzDztWMD1N5zFkZ0O0M3rwIKyiadk0', '2021-12-07 11:05:11.917651');
INSERT INTO `django_session` VALUES ('eh745gcwg2j87m7fuaht6olt83ehubkq', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MiwidXNlcm5hbWUiOiJcdTZkNGJcdThiZDUifQ:1mpTVd:7wH96vUyyH8-cj9skyCNX3sMARWoHH5qVHmGI2ycPSw', '2021-12-07 10:58:45.539466');
INSERT INTO `django_session` VALUES ('ivet52n8oocxvjwksxp6yzg5v1fqruz1', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MSwidXNlcm5hbWUiOiJcdTVkMTRcdTY3MmZcdTY4ZWUifQ:1mpTQl:aB7hmV8oc5OH5it51xyL_wxS5qaGlNLpzqD6tnVa4_c', '2021-12-07 10:53:43.497978');
INSERT INTO `django_session` VALUES ('kf5brf6s8bly3ay3q3qn89j1qkchhqye', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MiwidXNlcm5hbWUiOiJcdTVkMTRcdTY3MmZcdTY4ZWUifQ:1mpTbe:oP8abUgRIq-VjofEXbxHVc_LjJc5cvbdGLPEE4JGTjk', '2021-12-07 11:04:58.381856');
INSERT INTO `django_session` VALUES ('toz8pv610mwbo9odjovueph6u887pwhn', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MSwidXNlcm5hbWUiOiJcdTVkMTRcdTY3MmZcdTY4ZWUifQ:1mpTQl:aB7hmV8oc5OH5it51xyL_wxS5qaGlNLpzqD6tnVa4_c', '2021-12-07 10:53:43.498972');
INSERT INTO `django_session` VALUES ('utozqad2u9r2eo0dc6xsc0bzm2415880', 'eyJfc2Vzc2lvbl9leHBpcnkiOjAsImlzX2xvZ2luIjp0cnVlLCJpZCI6MiwidXNlcm5hbWUiOiJcdTVkMTRcdTY3MmZcdTY4ZWUifQ:1mpTcA:WxH5l8-Pq8tYoe264UY1wgtl8_XvNYBV8qZEtcpE41s', '2021-12-07 11:05:30.631629');
